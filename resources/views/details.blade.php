
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ url('/') }}/css/app.css" rel="stylesheet">
    <link href="{{ url('/') }}/css/dropzone.css" rel="stylesheet"> 
    <link href="{{ url('/') }}/css/datepicker3.css" rel="stylesheet"> 

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>.col-md-8 {
    display:inline-block  !important;
}
/* The sidebar menu */
.sidenav {
    height: 100%; /* Full-height: remove this if you want "auto" height */
    width: 160px; /* Set the width of the sidebar */
    position: fixed; /* Fixed Sidebar (stay in place on scroll) */
    z-index: 1; /* Stay on top */
    top: 0; /* Stay at the top */
    left: 0;
    background-color: #111; /* Black */
    overflow-x: hidden; /* Disable horizontal scroll */
    padding-top: 20px;
}

/* The navigation menu links */
.sidenav a {
    padding: 6px 8px 6px 16px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
}

/* When you mouse over the navigation links, change their color */
.sidenav a:hover {
    color: #f1f1f1;
}

/* Style page content */
.main {
    margin-left: 160px; /* Same as the width of the sidebar */
    padding: 0px 10px;
}

/* On smaller screens, where height is less than 450px, change the style of the sidebar (less padding and a smaller font size) */
@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}
    </style>
  </head>

  <body>
  <div class="sidenav">
  <a href="{{ url('/') }}">Home</a>
  <a href="#services">About Us</a>
</div>
    <div id="app">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
            <div style="margin-top:30px;"></div>
                <img style="width:150px;"src="../storage/app/public/{{$user->users_profile_picture}}"/>
                <span class="col-md-4 help-block"></span>
                <div class="form-group" id="form-group-name">
                  Phone
                  <span class="col-md-4 help-block">{{$user->users_phone}}</span>
              </div>
              <div class="form-group" id="form-group-phone">
                  Name
                <span class="col-md-4 help-block">{{$user->users_name}}</span>
              </div>
              <div class="form-group" id="form-group-email">
                Email
                <span class="col-md-4 help-block">{{$user->users_email}}</span>
              </div>
              <div class="form-group" id="form-group-gender">
                Gender
                <span class="col-md-4 help-block">{{$user->users_gender}}</span>
              </div>
                <div class="form-group" id="form-group-dob">
                Date of Birth
                <span class="col-md-4 help-block">{{$user->users_dob}}</span>
              </div>
                <div class="form-group" id="form-group-biography">
                Biography
                <span class="col-md-4 help-block">{{$user->users_biography}}</span>
              </div>
              
              <a type="button" href="{{ url('/edit') }}">Edit</a>
            </p>
                </form>
            </div>
            
        </div>
    </div>
      </div>
  


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ asset('js/dropzone.js')}}"></script>
    <script src="{{ asset('js/app.js')}}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.js')}}"></script>
    <script>
     $(document).ready(function () {
     
          });
    </script>
  </body>
</html>
