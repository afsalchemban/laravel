
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Starter Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ url('/') }}/css/app.css" rel="stylesheet">
    <link href="{{ url('/') }}/css/dropzone.css" rel="stylesheet"> 
    <link href="{{ url('/') }}/css/datepicker3.css" rel="stylesheet"> 

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="starter-template.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>.col-md-8 {
    display:inline-block  !important;
}

.sidenav {
    height: 100%; 
    width: 160px; 
    position: fixed; 
    z-index: 1; 
    top: 0; 
    left: 0;
    background-color: #111; 
    overflow-x: hidden; 
    padding-top: 20px;
}

.sidenav a {
    padding: 6px 8px 6px 16px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
}

.sidenav a:hover {
    color: #f1f1f1;
}
.main {
    margin-left: 160px; 
    padding: 0px 10px;
}

@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}
.help-block
{
  color:red;
}
    </style>
  </head>

  <body>
  <div class="sidenav">
  <a href="{{ url('/') }}">Home</a>
  <a href="#services">About Us</a>
</div>
    <div id="app">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
            <div style="margin-top:30px;"></div>
                <form action="{{ action('UsersController@upload') }}" class="col-md-4 dropzone" id="profile">
                <div class="dz-message" data-dz-message><span>Click here to upload your profile pic</span></div>
                  @csrf
                  <div class="fallback">
                    <input name="file" type="file" multiple />
                  </div>
                </form>
                <span class="col-md-4 help-block"></span>
                <form id="userForm" method="post" enctype="multipart/form-data">
                <div class="form-group" id="form-group-name">
                  <input type="text" class="col-md-8 form-control" value="{{$user->users_name}}" id="name" name="name" placeholder="Name"/>
                  <span class="col-md-4 help-block"></span>
              </div>
              <div class="form-group" id="form-group-phone">
                <input  type="text" class="col-md-8 form-control" value="{{$user->users_phone}}" id="phone" name="phone" placeholder="Phone"/>
                <span class="col-md-4 help-block"></span>
              </div>
              <div class="form-group" id="form-group-email">
                <input type="text" class="col-md-8 form-control" value="{{$user->users_email}}"  id="email" name="email" placeholder="Email"/>
                <span class="col-md-4 help-block"></span>
              </div>
              <div class="form-group" id="form-group-gender">
                <select class="col-md-8 form-control" id="gender" value="{{$user->users_gender}}"  name="gender" placeholder="Gender">
                <option value="">Gender</option>
                <option  value="male" @if($user->users_gender=='male') selected @endif >Male</option>
                <option value="female" @if($user->users_gender=='female') selected @endif >Female</option>
                </select>
                <span class="col-md-4 help-block"></span>
              </div>
                <div class="form-group" id="form-group-dob">
                <input type="text" class="datepicker col-md-8 form-control" value="{{$user->users_dob}}"  id="dob" name="dob" placeholder="Date of Birth"/>
                <span class="col-md-4 help-block"></span>
              </div>
                <div class="form-group" id="form-group-biography">
                <textarea  type="text" class="col-md-8 form-control"  id="biography" name="biography" placeholder="Biography">{{$user->users_biography}}</textarea>
                <span class="col-md-4 help-block"></span>
              </div>
              
              <input value="Update" id="submit" type="button">  
            </p>
                </form>
            </div>
            
        </div>
    </div>
      </div>
  


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="{{ asset('js/dropzone.js')}}"></script>
    <script src="{{ asset('js/app.js')}}"></script>
    <script src="{{ asset('js/bootstrap-datepicker.js')}}"></script>
    <script>
     $(document).ready(function () {
      $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
      });
function showValidationErrors(name, error) {
    var group = $("#form-group-" + name);
    console.log("#form-group-" + name);
    group.addClass('has-error');
    group.find('.help-block').text(error);
}
function clearValidationError(name) {
            var group = $("#form-group-" + name);
            group.removeClass('has-error');
            group.find('.help-block').text('');
        }
        $("#name, #phone, #email, #gender, #dob, #biography").on('keyup', function () {
            clearValidationError($(this).attr('id').replace('#', ''))
        });
      
    $.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					}
				});
            $(document).on('click','#submit',function(){
					
							
                    var url = "{{url('/validate')}}";
                    $.ajax({
                            type: 'post',
                            url: url,
                            data: $('#userForm').serialize(),
                            dataType: 'json',
                            success: function (data) {
                                window.location.href="{{url('/update')}}";
                            },
                            error: function (res) {
                              if (res.status == 422) {
                                  var data = res.responseJSON;
                                  for (let i in data['errors']) {
                                      showValidationErrors(i, data['errors'][i][0])
                                  }
                              }
                            }
                    });
            
            });
          });
    </script>
  </body>
</html>
