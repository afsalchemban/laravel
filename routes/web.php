<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UsersController@welcome');
Route::post('validate', 'UsersController@form_validate');
Route::get('profile', 'UsersController@profile');
Route::post('upload', 'UsersController@upload');
Route::get('save', 'UsersController@save');
Route::get('update', 'UsersController@update');
Route::get('detail', 'UsersController@detail');
Route::get('edit', 'UsersController@edit');