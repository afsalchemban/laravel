<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    public function welcome(Request $request)
    {
        $request->session()->forget('id');
        return view('welcome');
    }
    
    public function form_validate(Request $request)
    {
        // set form validation rules
        $this->validate($request, [
            'phone' => 'required',
            'name' => 'required',
            'email' => 'required|email',
            'gender' => 'required',
            'dob' => 'required',
            'biography'  => 'required|max:299|min:20'
        ]);
        
        $request->session()->put('phone', $request->input('phone'));
        $request->session()->put('name', $request->input('name'));
        $request->session()->put('email', $request->input('email'));
        $request->session()->put('gender', $request->input('gender'));
        $request->session()->put('dob', $request->input('dob'));
        $request->session()->put('biography', $request->input('biography'));
        return response()->json();

        // if the validation passes, save to database and redirect
    }
    public function upload(Request $request)
    {
        // set form validation rules
        $this->validate($request, [
            'file' => 'required|image',
        ]);
        $profile= $request->file('file')->store(
            'image', 'public'
           );
        $request->session()->put('img_path', $profile);
    }
    public function save(Request $request)
    {
        $user = new User;
        
        $user->users_phone = $request->session()->get('phone');
        $user->users_name = $request->session()->get('name');
        $user->users_email = $request->session()->get('email');
        $user->users_gender = $request->session()->get('gender');
        $user->users_dob = $request->session()->get('dob');
        $user->users_biography =$request->session()->get('biography');
        $user->users_profile_picture  = $request->session()->get('img_path');
        $request->session()->flush();
        $user->save();
        $request->session()->put('id', $user->id);
        return redirect()->to('detail');
        
    }
    public function update(Request $request)
    {
        $user=User::find($request->session()->get('id'));
        
        $user->users_phone = $request->session()->get('phone');
        $user->users_name = $request->session()->get('name');
        $user->users_email = $request->session()->get('email');
        $user->users_gender = $request->session()->get('gender');
        $user->users_dob = $request->session()->get('dob');
        $user->users_biography =$request->session()->get('biography');
        if($request->session()->has('img_path'))
        {
        $user->users_profile_picture  = $request->session()->get('img_path');
        }
        $request->session()->flush();
        $user->save();
        $request->session()->put('id', $user->id);
        return redirect()->to('detail');
        
    }
    public function detail(Request $request)
    {
        if($request->session()->has('id'))
        {
            $user=User::find($request->session()->get('id'));
            $data=array(
                'user' => $user
            );
            return view('details',$data);
        }
        else{
            return redirect()->to('/'); 
        }
        
    }
    public function edit(Request $request)
    {   
        if($request->session()->has('id'))
        {
            $user=User::find($request->session()->get('id'));
            $data=array(
                'user' => $user
            );
            return view('edit',$data);
        }
        else{
            return redirect()->to('/'); 
        }

    }
}
